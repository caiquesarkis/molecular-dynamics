function particle(id){
    // Variable initialization
    this.time = 1;
    this.sides = 28;
    this.radius = 10;
    this.id = id;
    this.pos = new THREE.Vector3(0,0,0);
    this.oldPos = new THREE.Vector3(0,0,0);
    this.a = new THREE.Vector3(0,0,0);

    this.t = 0;
    this.i = 1;
    this.dt = 0.02;
    this.mass = 1;
    this.k = 1;
    

    // Set the mesh of a circle and add it to the scene
    this.geometry = new THREE.CircleGeometry( this.radius, this.sides );
    this.material = new THREE.MeshBasicMaterial( { color: 0xffff00 } );
    this.circle = new THREE.Mesh( this.geometry, this.material );
    this.material.color.setRGB ( Math.random(),Math.random(),Math.random() );
    scene.add( this.circle );

    

    // Methods
    this.move = function (){
        // Calculate the aceleration
        
        //this.acelerations()

        // Knematic equation
        let x_next =  2* this.pos.x - this.oldPos.x + this.a.x* this.dt**2;
        let y_next = 2* this.pos.y - this.oldPos.y + this.a.y* this.dt**2;
        
        this.oldPos.y = this.pos.y;
        this.pos.y = y_next;
        this.circle.position.y  = y_next;  

        this.oldPos.x = this.pos.x;
        this.pos.x = x_next;
        this.circle.position.x  = x_next;  

        this.t += this.dt;
        this.a= new THREE.Vector3(0,0,0);
    }

    this.acelerations = function(){
        // Harmonic Oscilator solution
        //this.a = -(this.k/this.mass)* this.pos.x

        
        
        
    }

    


}


