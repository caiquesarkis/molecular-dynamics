// Global variables



const scene = new THREE.Scene();
// Add the ortographic camera to the scene
const camera = new THREE.OrthographicCamera( window.innerWidth/ - 2,window.innerWidth / 2, window.innerHeight / 2, window.innerHeight / - 2, 1, 1000 );
camera.position.z = 20;
scene.add( camera );

// Draw a axis to show the 2d space
const axesHelper = new THREE.AxesHelper( 1000 );
scene.add( axesHelper );

// Initialize the canvas
const renderer = new THREE.WebGLRenderer({antialias:true,alpha:true});
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );

// Create the particles by pressing a button on the screen
let gas = []
let aux = -1;
let aux2 = -1;
let aux3 = 0;

function creation(){
    while(aux < 20){
        aux += 1;
        aux2 +=1;
        
       
    
        
        gas.push(new particle(aux));
        gas[aux].pos.x = aux2*24
        gas[aux].oldPos.x = aux2*24
        if (aux2 %4 != 0 ){
            aux2 = -1;
            aux3 +=1;
        }
        gas[aux].pos.y = aux3*22;
        gas[aux].oldPos.y = aux3*22;
       
        
        

    }
    
} 






// Light config
let light = new THREE.PointLight(0xFFFFFF,1,500);
light.position.set(10,0,25);
const ambient = new THREE.AmbientLight(0x404040,5);
scene.add(ambient);

let stepsPerFrame = 25;
// Main animation loop
function animate(){
    requestAnimationFrame(animate)
            // Dynamic of the particles 
            for (var step=0; step<stepsPerFrame; step++) {
                // Sum of the forces caused by Lennard Jones potential
                for (let i = 0; i<gas.length; i++){
                    // Verify if there is more the one particle to start calculating interactions
                    if (gas.length > 1){
                        for (let j = 0; j <gas.length;j++){
                            if (i != j){
                                let x_dist = gas[i].pos.x - gas[j].pos.x
                                let y_dist = gas[i].pos.y - gas[j].pos.y
                                
                                let distSqrd = Math.sqrt(x_dist*x_dist + y_dist*y_dist)
                                let distSqrd_x = x_dist/distSqrd**2
                                let distSqrd_y = y_dist/distSqrd**2

                                
                                gas[i].a.x += 24*( (2*gas[i].radius/distSqrd)**12 -(2*gas[i].radius/distSqrd)**6)*distSqrd_x
                                gas[i].a.y += 24*( (2*gas[i].radius/distSqrd)**12 -(2*gas[i].radius/distSqrd)**6)*distSqrd_y
                            }
                        }
                }
                    
                    gas[i].move()

                }
            }
            
           
        
        
    
    renderer.render(scene,camera);
}

animate()